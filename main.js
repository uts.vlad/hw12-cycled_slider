const images = document.getElementsByClassName('images');
let counter = 1;
const start = document.querySelector('#start');
const stop = document.querySelector('#stop');

const changePick=()=>{
    if (counter ===0) images[images.length-1].classList.remove("--visible");
    images[counter].classList.toggle("--visible", true);
    if (images[counter-1])  images[counter-1].classList.remove("--visible");
    counter++;
    console.log(counter);
    if (counter >= images.length) {counter = 0;  return}
};


let timerId;
setTimer=()=>{
timerId = setInterval(changePick, 3000);
};
unSetTimer=()=>{
    clearInterval(timerId);
};

start.addEventListener('click', setTimer);
stop.addEventListener('click', unSetTimer);

